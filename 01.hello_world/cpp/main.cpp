#include <QApplication>
#include <QMainWindow>
#include <QLabel>

/* Simplesmente rodar 'qmake && make && open ./hello_world.app'
 * Arquivo 'pro' pode ser criado utilizando 'qmake -project'
*/
int main(int argc, char **argv) {
    QApplication app(argc, argv);
    app.setApplicationName("Hello Byne");

    QMainWindow window;
    window.setWindowTitle("Byne");
    window.resize(640, 480);

    QLabel *label = new QLabel("Hello Byne!");
    label->setAlignment(Qt::AlignCenter);

    window.setCentralWidget(label);
    window.show();

    return app.exec();
}
