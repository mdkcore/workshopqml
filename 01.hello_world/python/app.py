#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QLabel
from PyQt5.QtCore import Qt

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setApplicationName("Hello Byne")

    window = QMainWindow()
    window.setWindowTitle("Byne")
    window.resize(604, 480)

    label = QLabel("Hello Byne!")
    label.setAlignment(Qt.AlignCenter)

    window.setCentralWidget(label)
    window.show()

    sys.exit(app.exec_())
