#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# models example based on 'examples/quick/models/' from PyQt5

import sys
import os

from PyQt5.QtWidgets import QApplication
from PyQt5.QtQuick import QQuickView
from PyQt5.QtCore import QUrl

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setApplicationName("Hello Byne Models")

    view = QQuickView()
    view.setTitle("ByneModels")

    # defining models on QML context
    context = view.rootContext()

    # StringListModel
    import models.stringlistmodel as StringListModel
    context.setContextProperty('beersModel', StringListModel.dataList)


    # AbstractItemModel
    # from models.abstractitemmodel import Beer, BeerModel
    # model = BeerModel()
    # model.addBeer(Beer("Cheval Blanc", "Ale"))
    # model.addBeer(Beer("Murphy's Irish Stout", "Stout"))
    # model.addBeer(Beer("Negra Modelo", "Dark Lager"))

    # context.setContextProperty('beersModel', model)


    # ObjectListModel
    # from models.objectlistmodel import BeerObject
    # dataList = [
        # BeerObject("Cheval Blanc", "Ale"),
        # BeerObject("Murphy's Irish Stout", "Stout"),
        # BeerObject("Negra Modelo", "Dark Lager")
    # ]

    # context.setContextProperty('beersModel', dataList)
    ##

    view.setSource(QUrl.fromLocalFile(
        os.path.join(os.path.dirname(__file__), 'qml/main.qml')))

    view.resize(640, 480)
    view.show()

    sys.exit(app.exec_())
