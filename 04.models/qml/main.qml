import QtQuick 2.4

Item {
    id: root

    anchors.fill: parent

    ListView {
        anchors.fill: parent

        model: beersModel
        // delagate basico
        delegate: Text { text: "Beer: " + modelData } // StringListModel
        //delegate: abstractItemModelDelegate // AbstractItemModel
        //delegate: objectListModelDelegate // ObjectListModel
    }

    // delegate separado em outro componente
    Component {
        id: abstractItemModelDelegate

        Text {
            text: "Beer name: " + name + " | Type: " + type
        }
    }

    // delegate separado em outro componente, mais elaborado
    Component {
        id: objectListModelDelegate

        Row {
            spacing: 100

            Text {
                text: "<b>Beer name:</b> %1".arg(name)
            }

            Text {
                text: "<i>Type:</i> %1".arg(type)
            }
        }
    }
}
