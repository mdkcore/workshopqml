#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from PyQt5.QtCore import QAbstractListModel, QModelIndex, Qt, QUrl, QVariant

class Beer(object):

    def __init__(self, name, type):
        self._name = name
        self._type = type

    def name(self):
        return self._name

    def type(self):
        return self._type


class BeerModel(QAbstractListModel):

    NameRole = Qt.UserRole + 1
    TypeRole = Qt.UserRole + 2

    _roles = {NameRole: "name", TypeRole: "type"}

    def __init__(self, parent=None):
        super(BeerModel, self).__init__(parent)

        self._beers = []

    def addBeer(self, beer):
        self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
        self._beers.append(beer)
        self.endInsertRows()

    def rowCount(self, parent=QModelIndex()):
        return len(self._beers)

    def data(self, index, role=Qt.DisplayRole):
        try:
            beer = self._beers[index.row()]
        except IndexError:
            return QVariant()

        if role == self.NameRole:
            return beer.name()

        if role == self.TypeRole:
            return beer.type()

        return QVariant()

    def roleNames(self):
        return self._roles
