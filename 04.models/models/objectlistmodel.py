#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from PyQt5.QtCore import pyqtProperty, pyqtSignal, QObject, QUrl

class BeerObject(QObject):
    nameChanged = pyqtSignal()
    typeChanged = pyqtSignal()

    def __init__(self, name='', type='', parent=None):
        super(BeerObject, self).__init__(parent)

        self._name = name
        self._type = type

    @pyqtProperty(str, notify=nameChanged)
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        if self._name != name:
            self._name = name
            self.nameChanged.emit()

    @pyqtProperty(str, notify=typeChanged)
    def type(self):
        return self._type

    @type.setter
    def type(self, type):
        if self._type != type:
            self._type = type
            self.typeChanged.emit()
