#include <QApplication>
#include <QQuickView>
#include <QUrl>

//Simplesmente rodar 'qmake && make && open ./hello_world_qml.app'
int main(int argc, char **argv) {
    QApplication app(argc, argv);
    app.setApplicationName("Hello Byne");

    QQuickView view;

    view.setTitle("ByneQML");

    view.setSource(QUrl("../qml/main.qml"));

    view.resize(640, 480);
    view.show();

    return app.exec();
}
