import QtQuick 2.4

Item {
    id: root

    anchors.fill: parent

    Rectangle {
        anchors.fill: parent

        color: "cyan"
    }

    Text {
        anchors.fill: parent

        text: "Hello Byne QML!"
        color: "#99b5ca"

        font.pointSize: 36

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        renderType: Text.QtRendering // bad MacOS font rendering
    }
}
