#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import os

from PyQt5.QtWidgets import QApplication
from PyQt5.QtQuick import QQuickView
from PyQt5.QtCore import QUrl

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setApplicationName("Hello Byne")

    view = QQuickView()
    view.setTitle("ByneQML")

    view.setSource(QUrl.fromLocalFile(
        os.path.join(os.path.dirname(__file__), '../qml/main.qml')))

    view.resize(640, 480)
    view.show()

    sys.exit(app.exec_())
