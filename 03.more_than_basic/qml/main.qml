import QtQuick 2.4

Item {
    id: root

    anchors.fill: parent

    Text {
        id: message // identificador para acesso ao elemento

        anchors {
            top: parent.top
            topMargin: 60
            left: parent.left
            horizontalCenter: parent.horizontalCenter
        }

        color: "red" // utiliza SVG color names ou HTML codes

        font.pointSize: 24

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        renderType: Text.QtRendering // bad MacOS font rendering
    }

    Button {
        anchors {
            top: message.bottom
            left: parent.left
            centerIn: parent
        }

        width: 150
        height: 80

        // TODO uncomment me
        //color: "gray"
        //text: "Click Me!"
        //textSize: 20

        onClicked: {
            console.log("Mensagem na console");
            message.text = "Clicaram em mim! :D"
            //message.text = "Clicaram em mim %1 vezes :D".arg(count); // UNCOMMENT
        }
    }
}
