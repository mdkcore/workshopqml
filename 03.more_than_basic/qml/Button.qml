import QtQuick 2.4

Rectangle {
    id: root

    color: "white"

    property alias text: label.text // propriedade fazendo referencia a outra
    property int textSize: 12 // declaracao de uma nova propriedade

    property int _count: 0 // nao ha propriedades privadas

    signal clicked() // sinal a ser emitido, que pode ser conectado depois
    //signal clicked(var count) // UNCOMMENT

    function incrementCounter() {
        root._count++;
    }

    border {
        color: "black"
        width: 2
    }

    Text {
        id: label

        anchors.fill: parent

        color: "black"

        font.pointSize: root.textSize // property binding

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        renderType: Text.QtRendering
    }

    MouseArea {
        anchors.fill: parent

        onClicked: root.clicked()
        //onClicked: { // UNCOMMENT
            //incrementCounter();
            //root.clicked(root._count);
        //}
    }
}
