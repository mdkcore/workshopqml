// based on http://doc.qt.io/qt-5/qml-qtquick-flipable.html
import QtQuick 2.4

Item {
    id: root

    anchors.fill: parent

    Flipable {
        id: flipable

        property bool flipped: false

        anchors.fill: parent

        front: Image {
            anchors.centerIn: parent

            source: "../images/ai_que_delicia_do_campo.jpg"
        }

        back: Image {
            anchors.centerIn: parent

            source: "../images/padamim_sevilha.png"
            fillMode: Image.PreserveAspectFit

            width: 528
        }

        transform: Rotation {
            id: rotation
            origin {
                x: flipable.width / 2
                y: flipable.height / 2
            }

            axis {
                x: 0
                y: 1
                z: 0
            }

            angle: 0
        }

        states: State {
            name: "back"

            PropertyChanges { target: rotation; angle: 180 }
            when: flipable.flipped
        }

        transitions: Transition {
            NumberAnimation { target: rotation; property: "angle"; duration: 4000 }
        }

        MouseArea {
            anchors.fill: parent

            onClicked: flipable.flipped = !flipable.flipped
        }
    }
}
